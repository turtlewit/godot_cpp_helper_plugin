#include <stdio.h>

#include "exec.hpp"

using namespace godot;

void Exec::_register_methods()
{
	register_signal<Exec>("output_line", "output", GODOT_VARIANT_TYPE_STRING);
	register_signal<Exec>("process_exit", "return_code", GODOT_VARIANT_TYPE_INT);

	register_method("exec", &Exec::exec);
	register_method("exec_thread", &Exec::exec_thread);
	register_property("output", &Exec::output, String());
	register_property("thread", &Exec::thread, Ref<Thread>(), GODOT_METHOD_RPC_MODE_DISABLED, static_cast<godot_property_usage_flags>(0));
}

void Exec::_init()
{
	thread = Ref(Thread::_new());
}

void Exec::exec(godot::String command)
{
	output = "";
	if (thread->is_active())
		thread->wait_to_finish();
	thread->start(this, "exec_thread", command);
}

void Exec::exec_thread(godot::Variant userdata)
{
	String command = static_cast<String>(userdata);
	CharString cs_command = command.utf8();

#if defined(__linux__) || defined(__APPLE__)
	FILE* f = popen(cs_command.get_data(), "r");
#elif defined(__WIN32__)
	FILE* f = _popen(cs_command.get_data(), "r");
#else
	static_assert(false, "Current platform is unsupported.");
#endif
	char buf[65535];

	while (fgets(buf, 65535, f)) {
		String line = String(buf);
		output += line;
		call_deferred("emit_signal", "output_line", line);
	};

	int r = pclose(f);
	call_deferred("emit_signal", "process_exit", r);
}
