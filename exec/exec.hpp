#pragma once

#include <Godot.hpp>
#include <Node.hpp>
#include <Thread.hpp>

class Exec : public godot::Node {
	GODOT_CLASS(Exec, godot::Node)

public:
	static void _register_methods();

	Exec()
	{
	}

	~Exec()
	{
	}

	void _init();

	void exec(godot::String command);
	void exec_thread(godot::Variant userdata);

private:
	godot::Ref<godot::Thread> thread;
	godot::String output = "";
};
